vim.cmd("set number")
vim.cmd("set syntax")
vim.cmd("set nowrap")
vim.cmd("set noexpandtab")
vim.cmd("set autoindent")
-- vim.cmd("set tapstop=2")
-- vim.cmd("set softtapstop=2")
vim.cmd("set shiftwidth=2")

vim.g.loaded_netrw = 1
vim.g.loaded_netrwPlugin = 1
vim.g.mapleader = " "
