return {
  --	{ "tanvirtin/vgit.nvim", requires = { "nvim-lua/plenary.nvim", "nvim-tree/nvim-web-devicons" }, },
  -- { "ms-jpq/coq_nvim" }, -- broken?
  {
    "vijaymarupudi/nvim-fzf",
    config = function() require("fzf") end,
  },
  { "eandrju/cellular-automaton.nvim" },
  { "nvim-lua/plenary.nvim" },
  { "windwp/nvim-autopairs", event = "InsertEnter", opts={} }, 
  { "sudormrfbin/cheatsheet.nvim", dependencies = { "nvim-telescope/telescope.nvim", "nvim-lua/popup.nvim", "nvim-lua/plenary.nvim" } },
}
