-- Splash screen

local logo_spooky = {
  [[ ▄▀▀▄ ▀▄  ▄▀▀█▄▄▄▄  ▄▀▀▀▀▄   ▄▀▀▄ ▄▀▀▄  ▄▀▀█▀▄    ▄▀▀▄ ▄▀▄  ]],
  [[ █  █ █ █ ▐  ▄▀   ▐ █      █ █   █    █ █   █  █  █  █ ▀  █ ]],
  [[ ▐  █  ▀█   █▄▄▄▄▄  █      █ ▐  █    █  ▐   █  ▐  ▐  █    █ ]],
  [[   █   █    █    ▌  ▀▄    ▄▀    █   ▄▀      █       █    █  ]],
  [[ ▄▀   █    ▄▀▄▄▄▄     ▀▀▀▀       ▀▄▀     ▄▀▀▀▀▀▄  ▄▀   ▄▀   ]],
  [[ █    ▐    █    ▐                       █       █ █    █    ]],
  [[ ▐         ▐                            ▐       ▐ ▐    ▐    ]]
}

local commands = {
  { icon = '󰊳 ', desc = 'Update', group = '@property', action = 'Lazy update', key = 'u' },
  { icon = ' ', desc = 'New File', key = 'e', keymap = '', action = 'lua print("todo")'},
  { icon = '󰈞 ', desc = 'Find File', key = 'b', keymap = 'SPC f f', action = 'Telescope find_files' },
  { icon = '󰈬 ', desc = 'Find Word', key = 'g', keymap = 'SPC f g', action = 'Telescope live_grep' },
  { icon = ' ', desc = 'Bookmarks', key = 'b', keymap = 'SPC f m', action = 'lua print("todo")' },
  { icon = '󰊄 ', desc = 'Recent Files', key = 'h', keymap = 'SPC f h', action = 'lua print("todo")' },
  --{ icon = ' ', desc = 'Find Dotfiles', key = 'd', keymap = 'SPC f d', action = 'Telescope dotfiles' },
  { icon = ' ', desc = 'Last Session', key = 's', keymap = 'SPC s l', action = 'lua print("todo")'},
  { icon = '? ', desc = 'Help', key = '?', keymap = 'SPC f h', action = 'Telescope help_tags' },
}

local dashboard_config = {
  theme = 'doom',
  config = {
    week_header = {
      enable = true,
    },
    --header = {}, --your header
    center = commands,
    footer = {}  --your footer
  }
}

local dashboard = {
  'nvimdev/dashboard-nvim',
  event = 'VimEnter',
  config = function()
    local dashboard = require('dashboard')
    dashboard.setup( dashboard_config )
  end,
  requires = {'nvim-tree/nvim-web-devicons'}
}

return {
  dashboard,
}
