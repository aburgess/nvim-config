return {
  {
    "rcarriga/nvim-dap-ui",
    dependencies = {"mfussenegger/nvim-dap", "nvim-neotest/nvim-nio"}
  },
  {
    "mfussenegger/nvim-dap",
    dependencies = {
      "rcarriga/nvim-dap-ui",
      "leoluz/nvim-dap-go",	-- Go Debugger
    },
    config = function ()
      local dap, dapui = require("dap"), require("dapui")
      require( "dapui" ).setup()
      require( "dap-go" ).setup()

      --Set up event listeners
      dap.listeners.before.attach.dapui_config = function() dapui.open() end
      dap.listeners.before.launch.dapui_config = function() dapui.open() end
      dap.listeners.before.event_terminated.dapui_config = function() dapui.close() end
      dap.listeners.before.event_exited.dapui_config = function() dapui.close() end

      -- Set up all keybinds
      vim.keymap.set('n', '<F5>', dap.continue )
      vim.keymap.set('n', '<F10>', dap.step_over )
      vim.keymap.set('n', '<F11>', dap.step_into )
      vim.keymap.set('n', '<F12>', dap.step_out )
      vim.keymap.set('n', '<Leader>dt', dap.toggle_breakpoint )
      vim.keymap.set('n', '<Leader>dc', dap.continue )
      vim.keymap.set('n', '<Leader>dB', dap.set_breakpoint )
    end,
  },

}
