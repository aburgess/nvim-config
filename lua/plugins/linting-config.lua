return {
  {
    "nvimtools/none-ls.nvim",
    config = function()
      local null_ls = require("null-ls")
      local options = {

	-- Formatters
	null_ls.builtins.formatting.stylua,		-- Lua
	null_ls.builtins.formatting.prettier,		-- Web / JS / TS / HTML / CSS / GraphQL / Markdown / YAML
	null_ls.builtins.formatting.black,		-- Python
	-- Diagnostics
	null_ls.builtins.diagnostics.checkmake,		-- make
	null_ls.builtins.diagnostics.cppcheck,		-- C/C++
	null_ls.builtins.diagnostics.dotenv_linter,	-- .env files
	null_ls.builtins.diagnostics.hadolint,		-- dockerfiles
	null_ls.builtins.diagnostics.pylint,		-- python
	null_ls.builtins.diagnostics.textidote,		-- Spelling, grammar and style checking on LaTeX documents.
	--null_ls.builtins.diagnostics.eslint,		-- Javascript / Typescript
	--null_ls.builtins.diagnostics.clang_check,	-- C / CPP
      }
      null_ls.setup(options)

      vim.keymap.set('n', '<leader>gf', vim.lsp.buf.format, {})
    end,
  }
}
