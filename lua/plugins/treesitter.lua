return {
	"nvim-treesitter/nvim-treesitter",
	build = ":TSUpdate",
	config = function()
		local config = require("nvim-treesitter.configs")
		config.setup({
			auto_install = true,
			ensure_installed = {
				"lua",
				"javascript",
				"c",
				"cpp",
				"dockerfile",
				"gitignore",
				"go",
				"json",
				"latex",
				"markdown",
				"make",
				"proto",
				"python",
				"rust",
				"scss",
				"sql",
				"typescript",
				"yaml"
			},
			sync_install = false,
			highlight = { enabble = true },
			indent = { enabble = true },
		})
	end,
}
