return {
  {
    "williamboman/mason.nvim",
    config = function()
      local options = {
	PATH = "prepend",
      }
      require("mason").setup( options )
    end,
  }, {
    "williamboman/mason-lspconfig.nvim",
    config = function()
      local options = {
	-- See below for full list
	-- https://github.com/williamboman/mason-lspconfig.nvim?tab=readme-ov-file#setup
	ensure_installed = {
	  "lua_ls",		-- Lua
	  "clangd",		-- C / C++
	  "texlab",		-- LaTeX
	  "autotools_ls",	-- Make / Automake / Autoconf
	  "tsserver",		-- Typescript / Javascript
  	  "pyright",		-- Python
	},
	automatic_installation = true
      }
      require( "mason-lspconfig" ).setup( options )
    end,
  },
  {
    "neovim/nvim-lspconfig",
    config = function()
      local capabilities = require( "cmp_nvim_lsp" ).default_capabilities()

      local lspconfig = require("lspconfig")
      -- local lsp_server_options = { capabilities = capabilities }
      lspconfig.lua_ls.setup( { capabilities = capabilities } )
      lspconfig.clangd.setup( { capabilities = capabilities } )
      lspconfig.texlab.setup( { capabilities = capabilities } )
      lspconfig.tsserver.setup( { capabilities = capabilities } )
      lspconfig.pyright.setup( { capabilities = capabilities } )

      vim.keymap.set('n', 'gD', vim.lsp.buf.declaration, opts)
      vim.keymap.set('n', 'gd', vim.lsp.buf.definition, opts)
      vim.keymap.set('n', 'K', vim.lsp.buf.hover, opts)
      vim.keymap.set('n', 'gi', vim.lsp.buf.implementation, opts)
      vim.keymap.set('n', '<C-k>', vim.lsp.buf.signature_help, opts)
      vim.keymap.set({ 'n', 'v' }, '<leader>ca', vim.lsp.buf.code_action, opts)
      vim.keymap.set('n', 'gr', vim.lsp.buf.references, opts)
    end,
  }
}
