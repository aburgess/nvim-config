return {
  -- Icons
  { "nvim-tree/nvim-web-devicons" },

  -- The info bar along the bottom
  {
    "nvim-lualine/lualine.nvim",
    dependencies = { "nvim-tree/nvim-web-devicons" },
    lazy=false,
    config = function()
      local options = { theme = "dracula" }
      require( "lualine" ).setup( options );
    end,
  },

  -- the bar along the top
  {
    "romgrk/barbar.nvim",
    dependencies = {
      "lewis6991/gitsigns.nvim", -- OPTIONAL: for git status
      "nvim-tree/nvim-web-devicons", -- OPTIONAL: for file icons
    },
    init = function() vim.g.barbar_auto_setup = false end,
    opts = { },
    version = "^1.0.0", -- optional: only update when a new 1.x version is released
  },
}
